<?php
session_start();
include_once ('../vendor/autoload.php');
//
//use App\User;
use App\Utility\Debugger;


//if ($validate['validation_success']){
//$user = new User($validate['data']);
//}

$userData = [];
if (array_key_exists('userData', $_SESSION)){
    $userData = unserialize($_SESSION['userData']);

}
//print_r($_COOKIE['user']);
//echo count($userData);
//echo "<pre>";
//print_r($userData);
//echo "</pre>";
//echo count($storeData);

//$user = new User($storeData);
//Debugger::debug($userData);
?>




<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>User Details List</title>
</head>
<body>

<?php
//if(!$validate['validation_success']) {
//
//    foreach ($validate['data'] as $error){
//        echo $error.'<br>';
//    }
//}
//?>

<div class="container">
            <h1 style="text-align: center">User Details</h1>


            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Address</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>


                <?php
                if (count($userData) <= 0){

                    ?>
                    <tr>
                        <td colspan="4" style="text-align: center">No Record Found</td>
                    </tr>
                    <?php
                }else{
                    foreach ($userData as $key=> $user){

                ?>

                <tbody>
                <tr>
                    <td><a href="show.php?userId=<?php echo $key;?>"><?php echo $user['name']?></td>
                    <td><a href="show.php?userId=<?php echo $key;?>"><?php echo $user['email']?></td>
                    <td><a href="show.php?userId=<?php echo $key;?>"><?php echo $user['number']?></td>
                    <td><a href="show.php?userId=<?php echo $key;?>"><?php echo $user['address']?></td>
                    <td>
                        <a href="edit.php?userId=<?php echo $key;?>"><button type="button" class="btn btn-success">Edit</button></a>
                        <a href="delete.php?userId=<?php echo $key;?>" onclick="javascript:return confirm('Are You sure to delete?')"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
                </tbody>

             <?php
             }

}
             ?>
            </table>
    <a href="index.php" type="button" class="btn btn-success" >Add New</a>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>