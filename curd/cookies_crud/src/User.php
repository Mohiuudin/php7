<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/17/2020
 * Time: 11:33 AM
 */

namespace App;


class User
{

    public $name = null;
    public $email = null;
    public $number = null;
    public $address = null;

    public function __construct($userInformation)
    {
        if (array_key_exists('name', $userInformation)){
            $this->name = $userInformation['name'];
        }
        if (array_key_exists('email', $userInformation)){
            $this->email = $userInformation['email'];
        }
        if (array_key_exists('number', $userInformation)){
            $this->number = $userInformation['number'];
        }
        if (array_key_exists('address', $userInformation)){
            $this->address = $userInformation['address'];
        }
    }

}