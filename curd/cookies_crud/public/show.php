<?php
use App\Utility\Debugger;
    $userId = $_GET['userId'];

    $userData = unserialize($_COOKIE['userData']);

    $user = $userData[$userId];

?>



<html>
    <head>
        <title>Cookies CRUD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a style="font-size: 30px; font-weight: bold"  class="nav-link" href="view.php">All User</a>
                    </li>
                    <li class="nav-item">
                        <a style="font-size: 30px; font-weight: bold" class="nav-link" href="index.php">Create User</a>
                    </li>
                    </ul>
                </div>
            </nav>

            <div class="row">
                <h1 style="margin-left: 40%">Single Show</h1>

                <div class="col-xl-8 offset-xl-2 py-5">
                    <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="font-size: 18px; font-weight: bold;"  for="form_name">Name</label>
                                        : <?php echo $user['name']?>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style=" font-weight: bold; font-size: 18px" for="form_lastname">Email</label>
                                        : <?php echo $user['email']?>
                                        
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="font-weight: bold; font-size: 18px" for="form_email">Phone Number</label>
                                        : <?php echo $user['number']?>

                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="font-weight: bold; font-size: 18px" for="form_lastname">Address</label>
                                        : <?php echo $user['address']?>

                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
            </div>

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
    </body>
</html>