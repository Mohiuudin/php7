<?php
$books = simplexml_load_file('book.xml');

if(isset($_POST['submitSave'])) {

    foreach($books->book as $book){
        if($book['id']==$_POST['id']){
            $book->author = $_POST['author'];
            $book->title = $_POST['title'];
            $book->genre = $_POST['genre'];
            $book->price = $_POST['price'];
            $book->publish_date = $_POST['publish_date'];
            $book->description = $_POST['description'];
            break;
        }
    }
    file_put_contents('book.xml', $books->asXML());
    header('location:index.php');
}

foreach($books->book as $book){
    if($book['id']==$_GET['id']){
        $id = $book['id'];
        $author = $book->author;
        $title = $book->title;
        $genre = $book->genre;
        $price = $book->price;
        $publish_date = $book->publish_date;
        $description = $book->description;
        break;
    }
}

?>
<form method="post">
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td>Id</td>
            <td><input type="text" name="id" value="<?php echo $id; ?>" readonly="readonly"></td>
        </tr>
        <tr>
            <td>author</td>
            <td><input type="text" name="author" value="<?php echo $author; ?>"></td>
        </tr>
        <tr>
            <td>title</td>
            <td><input type="text" name="title" value="<?php echo $title; ?>"></td>
        </tr>
        <tr>
            <td>genre</td>
            <td><input type="text" name="genre" value="<?php echo $genre; ?>"></td>
        </tr>
        <tr>
            <td>price</td>
            <td><input type="text" name="price" value="<?php echo $price; ?>"></td>
        </tr>
        <tr>
            <td>publish date</td>
            <td><input type="date" name="publish_date" value="<?php echo $publish_date; ?>"></td>
        </tr>
        <tr>
            <td>description</td>
            <td><input type="text" name="description" value="<?php echo $description; ?>"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="Save" name="submitSave"></td>
        </tr>
    </table>
</form>