<?php
if(isset($_GET['action'])) {
    $books = simplexml_load_file('book.xml');
    $id = $_GET['id'];
    $index = 0;
    $i = 0;
    foreach($books->book as $book){
        if($book['id']==$id){
            $index = $i;
            break;
        }
        $i++;
    }
    unset($books->book[$index]);
    file_put_contents('book.xml', $books->asXML());
}
$books = simplexml_load_file('book.xml');
echo 'Number of books: '.count($books);
?>
<br>
<a href="add.php">Add new book</a>
<br>
<table cellpadding="2" cellspacing="2" border="1">
    <tr>
        <th>Id</th>
        <th>author</th>
        <th>title</th>
        <th>genre</th>
        <th>price</th>
        <th>publish_date</th>
        <th>description</th>
        <th>action</th>
    </tr>
    <?php foreach($books->book as $book) { ?>
        <tr>
            <td><?php echo $book['id']; ?></td>
            <td><?php echo $book->author; ?></td>
            <td><?php echo $book->title; ?></td>
            <td><?php echo $book->genre; ?></td>
            <td><?php echo $book->price; ?></td>
            <td><?php echo $book->publish_date; ?></td>
            <td><?php echo $book->description; ?></td>
            <td><a href="edit.php?id=<?php echo $book['id']; ?>">Edit</a> |
                <a href="index.php?action=delete&id=<?php echo $book['id']; ?>" onclick="return confirm('Are you sure?')">Delete</a></td>
        </tr>
    <?php } ?>
</table>