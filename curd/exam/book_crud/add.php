<?php
if(isset($_POST['submitSave'])) {
    $books = simplexml_load_file('book.xml');
    $book = $books->addChild('book');
    $book->addAttribute('id', $_POST['id']);
    $book->addChild('author', $_POST['author']);
    $book->addChild('title', $_POST['title']);
    $book->addChild('genre', $_POST['genre']);
    $book->addChild('price', $_POST['price']);
    $book->addChild('publish_date', $_POST['publish_date']);
    $book->addChild('description', $_POST['description']);
    file_put_contents('book.xml', $books->asXML());
    header('location:index.php');
}
?>
<form method="post">
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td>Id</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td>author</td>
            <td><input type="text" name="author"></td>
        </tr>
        <tr>
            <td>title</td>
            <td><input type="text" name="title"></td>
        </tr>
        <tr>
            <td>genre</td>
            <td><input type="text" name="genre"></td>
        </tr>
        <tr>
            <td>price</td>
            <td><input type="number" name="price"></td>
        </tr>
        <tr>
            <td>publish_date</td>
            <td><input type="date" name="publish_date"></td>
        </tr>
        <tr>
            <td>description</td>
            <td><input type="text" name="description"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="Save" name="submitSave"></td>
        </tr>
    </table>
</form>