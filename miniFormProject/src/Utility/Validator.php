<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/29/2020
 * Time: 12:08 PM
 */

namespace App\Utility;


class Validator
{

    public static function validate($nonValidate){

        $errors = [];
        $validate = null;;

        if ($nonValidate['childFirstName'] == ''){
            $errors[] = "Child first Name is not found. Please Provide First Name";
        }

        if ($nonValidate['childMiddleName'] == ''){
            $errors[] = "Child Middle Name is not found. Please Provide Middle Name";
        }

        if ($nonValidate['childLastName'] == ''){
            $errors[] = "Child Last Name is not found. Please Provide Last Name";
        }

        if ($nonValidate['childSuffix'] == ''){
            $errors[] = "Child Suffix Name is not found. Please Provide Suffix Name";
        }

        if ($nonValidate['childTimeBirth'] == ''){
            $errors[] = "Child Time Birth is not found. Please Provide Child Time Birth";
        }

        if ($nonValidate['genders'] == ''){
            $errors[] = "Child Gender is not found. Please Provide Child Gender";
        }

        if ($nonValidate['dateOfBirth'] == ''){
            $errors[] = "Child Date Of Birth is not found. Please Provide Child Date Of Birth";
        }

        if ($nonValidate['childFacilityName'] == ''){
            $errors[] = "Child Facility Name is not found. Please Provide Child Facility Name";
        }

        if ($nonValidate['childCityTown'] == ''){
            $errors[] = "Child City Town is not found. Please Provide Child City Town";
        }

        if ($nonValidate['childCountryBirth'] == ''){
            $errors[] = "Child Country Birth is not found. Please Provide Child Country Birth";
        }






        if ($nonValidate['motherFirstName'] == ''){
            $errors[] = "Mother first Name is not found. Please Provide First Name";
        }

        if ($nonValidate['motherMiddleName'] == ''){
            $errors[] = "Mother Middle Name is not found. Please Provide Middle Name";
        }

        if ($nonValidate['motherLastName'] == ''){
            $errors[] = "Mother Last Name is not found. Please Provide Last Name";
        }

        if ($nonValidate['motherSuffix'] == ''){
            $errors[] = "Mother Suffix Name is not found. Please Provide Suffix Name";
        }

        if ($nonValidate['motherTimeBirth'] == ''){
            $errors[] = "Mother Time Birth is not found. Please Provide Time Birth";
        }

        if ($nonValidate['motherBirthPlace'] == ''){
            $errors[] = "Birth Place Name is not found. Please Provide Place Name";
        }
        if ($nonValidate['motherState'] == ''){
            $errors[] = "Mother State Name is not found. Please Provide State Name";
        }

        if ($nonValidate['motherCityTown'] == ''){
            $errors[] = "Mother City Town is not found. Please Provide City Town";
        }

        if ($nonValidate['motherCountryBirth'] == ''){
            $errors[] = "Mother Country Birth is not found. Please Provide Country Birth";
        }
        if ($nonValidate['streetNumber'] == ''){
            $errors[] = "Mother Street and Number is not found. Please Provide Street Number";
        }
        if ($nonValidate['apartmentNo'] == ''){
            $errors[] = "Mother Apartment No is not found. Please Provide Apartment No";
        }

        if ($nonValidate['motherZipCode'] == ''){
            $errors[] = "Mother Zip Code is not found. Please Provide Zip Code";
        }

        if ($nonValidate['motherCityLimits'] == ''){
            $errors[] = "Mother City Limits is not found. Please Provide City Limits";
        }


        if ($nonValidate['fatherFirstName'] == ''){
            $errors[] = "Father First Name is not found. Please Provide First Name";
        }

        if ($nonValidate['fatherMiddleName'] == ''){
            $errors[] = "Father Middle Name is not found. Please Provide Middle Name";
        }

        if ($nonValidate['fatherLastName'] == ''){
            $errors[] = "Father Last Name is not found. Please Provide Last Name";
        }

        if ($nonValidate['fatherSuffix'] == ''){
            $errors[] = "Father Suffix is not found. Please Provide Suffix Name";
        }

        if ($nonValidate['fatherCertifierName'] == ''){
            $errors[] = "Father Certifier Name is not found. Please Provide Certifier Name";
        }

        if ($nonValidate['titles'] == ''){
            $errors[] = "Father Titles is not found. Please Provide Titles";
        }

        if ($nonValidate['fatherDateCertified'] == ''){
            $errors[] = "Father Date certified is not found. Please Provide certified date";
        }

        if ($nonValidate['fatherDateRegister'] == ''){
            $errors[] = "Father Date register is not found. Please Provide Field register date";
        }

        if ($nonValidate['phoneNo'] == ''){
            $errors[] = "Telephone Number is not found. Please Provide Telephone Number";
        }

        if ($nonValidate['referringHospital'] == ''){
            $errors[] = "Referring Hospital Address is not found. Please Provide Hospital Address";
        }



        if (count($errors) > 0){
            return ['validation_success' => false, 'data' => $errors];
        }

        $validate =  $nonValidate;
        return ['validation_success' => true, 'data' => $validate];


//        $validate = $nonValidate;
//        return $validate;
    }
}