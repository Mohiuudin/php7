<?php


namespace App\Persons;



class Father
{

    public $fatherFirstName = null;
    public $fatherMiddleName = null;
    public $fatherLastName = null;
    public $fatherSuffix = null;
    public $fatherDateBirth = null;
    public $fatherBirthPlace= null;
    public $fatherCertifierName = null;
    public $titles = null;
    public $fatherDateCertified = null;
    public $fatherDateRegister = null;
    public $phoneNo = null;
    public $referringHospital = null;


    public function __construct($fatherInformation)
    {
        $this->fatherFirstName = $fatherInformation['fatherFirstName'];
        $this->fatherMiddleName = $fatherInformation['fatherMiddleName'];
        $this->fatherLastName = $fatherInformation['fatherLastName'];
        $this->fatherSuffix = $fatherInformation['fatherSuffix'];
        $this->fatherDateBirth = $fatherInformation['fatherDateBirth'];
        $this->fatherBirthPlace = $fatherInformation['fatherBirthPlace'];
        $this->fatherCertifierName = $fatherInformation['fatherCertifierName'];
        $this->titles = $fatherInformation['titles'];
        $this->fatherDateCertified = $fatherInformation['fatherDateCertified'];
        $this->fatherDateRegister = $fatherInformation['fatherDateRegister'];
        $this->phoneNo = $fatherInformation['phoneNo'];
        $this->referringHospital = $fatherInformation['referringHospital'];

    }
}



