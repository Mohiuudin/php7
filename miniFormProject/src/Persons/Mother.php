<?php

namespace App\Persons;

class Mother
{

    public $motherFirstName = null;
    public $motherMiddleName = null;
    public $motherLastName = null;
    public $motherSuffix = null;
    public $motherTimeBirth = null;
    public $motherBirthPlace = null;
    public $motherState = null;
    public $motherCityTown = null;
    public $motherCountryBirth = null;
    public $streetNumber = null;
    public $apartmentNo = null;
    public $motherZipCode = null;
    public $motherCityLimits = null;

    public function __construct($motherInformation)
    {
        $this->motherFirstName = $motherInformation['motherFirstName'];
        $this->motherMiddleName = $motherInformation['motherMiddleName'];
        $this->motherLastName = $motherInformation['motherLastName'];
        $this->motherSuffix = $motherInformation['motherSuffix'];
        $this->motherTimeBirth = $motherInformation['motherTimeBirth'];
        $this->motherBirthPlace = $motherInformation['motherBirthPlace'];
        $this->motherState = $motherInformation['motherState'];
        $this->motherCityTown = $motherInformation['motherCityTown'];
        $this->motherCountryBirth = $motherInformation['motherCountryBirth'];
        $this->streetNumber = $motherInformation['streetNumber'];
        $this->apartmentNo = $motherInformation['apartmentNo'];
        $this->motherZipCode = $motherInformation['motherZipCode'];
        $this->motherCityLimits = $motherInformation['motherCityLimits'];
    }

}