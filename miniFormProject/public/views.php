<?php

include_once ('../vendor/autoload.php');
use App\Persons\Child;
use App\Persons\Mother;
use App\Persons\Father;
use App\Utility\Sanitizer;
use App\Utility\Validator;
use App\Utility\Utility;

if (!Utility::isPosted()){
    header("location:index.php");
}

$sanitize = Sanitizer::sanitize($_POST);
$validate = Validator::validate($sanitize);

if ($validate['validation_success']){
    $child = new Child($validate['data']);
    $mother = new Mother($validate['data']);
    $father = new Father($validate['data']);
}


?>




<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>US Stander Certificate of live birth</title>
</head>
<body>

<?php
if(!$validate['validation_success']) {

    foreach ($validate['data'] as $error){
        echo $error.'<br>';
    }
}
?>

<div class="container">
    <div class="row">
        <div class="col-4">
            <h1>Child Details</h1>
            <dl>
                <dt>First name</dt>
                <dd><?= !empty($child->childFirstName) ? $child->childFirstName : 'Not provided'; ?></dd>

                <dt>Middle name</dt>
                <dd><?= !empty($child->childMiddleName) ? $child->childMiddleName : 'Not provided'; ?></dd>

                <dt>Last name</dt>
                <dd><?= !empty($child->childLastName) ? $child->childLastName : 'Not provided'; ?></dd>

                <dt>Suffix name</dt>
                <dd><?= !empty($child->childSuffix) ? $child->childSuffix : 'Not provided'; ?></dd>

                <dt>Birth Time</dt>
                <dd><?= !empty($child->childTimeBirth) ? $child->childTimeBirth : 'Not provided'; ?></dd>

                <?php
                $genders = [];

                if (strtoupper(array_key_exists('genders', $_POST))) {
                    $genders = $_POST['genders'];
                }
                if (count($genders) == 0) {
                    echo "<dt>Gender</dt>";
                    echo "<dd>Not Provided</dd>";
                }
                foreach ($genders as $gender):
                    echo "<dt>Gender</dt>";
                    echo $gender;
                endforeach;

                ?>

                <dt>Date of Birth</dt>
                <dd><?= !empty($child->dateOfBirth) ? $child->dateOfBirth : 'Not provided'; ?></dd>

                <dt>Facility Name</dt>
                <dd><?= !empty($child->childFacilityName) ? $child->childFacilityName : 'Not provided'; ?></dd>

                <dt>City Town</dt>
                <dd><?= !empty($child->childCityTown) ? $child->childCityTown : 'Not provided'; ?></dd>

                <dt>Country Birth Town</dt>
                <dd><?= !empty($child->childCountryBirth) ? $child->childCountryBirth : 'Not provided'; ?></dd>


            </dl>
        </div>

       <div class="col-4">
            <h1>Mother Details</h1>
            <dl>
                <dt>First name</dt>
                <dd><?= !empty($mother->motherFirstName) ? $mother->motherFirstName : 'Not provided'; ?></dd>

                <dt>Middle name</dt>
                <dd><?= !empty($mother->motherMiddleName) ? $mother->motherMiddleName : 'Not provided'; ?></dd>

                <dt>Last name</dt>
                <dd><?= !empty($mother->motherLastName) ? $mother->motherLastName : 'Not provided'; ?></dd>

                <dt>Suffix name</dt>
                <dd><?= !empty($mother->motherSuffix) ? $mother->motherSuffix : 'Not provided'; ?></dd>

                <dt>Birth Time</dt>
                <dd><?= !empty($mother->motherTimeBirth) ? $mother->motherTimeBirth : 'Not provided'; ?></dd>

                <dt>Birth Place</dt>
                <dd><?= !empty($mother->motherBirthPlace) ? $mother->motherBirthPlace : 'Not provided'; ?></dd>

                <dt>Mother State</dt>
                <dd><?= !empty($mother->motherState) ? $mother->motherState : 'Not provided'; ?></dd>

                <dt>City Town</dt>
                <dd><?= !empty($mother->motherCityTown) ? $mother->motherCityTown : 'Not provided'; ?></dd>

                <dt>Country Birth Town</dt>
                <dd><?= !empty($mother->motherCountryBirth) ? $mother->motherCountryBirth : 'Not provided'; ?></dd>

                <dt>Street and Number</dt>
                <dd><?= !empty($mother->streetNumber) ? $mother->streetNumber : 'Not provided'; ?></dd>

                <dt>Apartment No</dt>
                <dd><?= !empty($mother->apartmentNo) ? $mother->apartmentNo : 'Not provided'; ?></dd>

                <dt>Zip Code</dt>
                <dd><?= !empty($mother->motherZipCode) ? $mother->motherZipCode : 'Not provided'; ?></dd>


                <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                    if (array_key_exists('motherCityLimits',$_POST)){
                        echo "<dt>Mother City Limits</dt>";
                        echo "you select"." ".$_POST['motherCityLimits'];
                    }else{
                        echo "<dt>Mother City Limits</dt>";
                        echo "please select something ";
                    }
                }
                ?>

            </dl>        
       </div>

        <div class="col-4">
            <h1>Father Details</h1>
            <dl>
                <dt>First name</dt>
                <dd><?= !empty($father->fatherFirstName) ? $father->fatherFirstName : 'Not provided'; ?></dd>

                <dt>Middle name</dt>
                <dd><?= !empty($father->fatherMiddleName) ? $father->fatherMiddleName : 'Not provided'; ?></dd>

                <dt>Last name</dt>
                <dd><?= !empty($father->fatherLastName) ? $father->fatherLastName : 'Not provided'; ?></dd>

                <dt>Suffix name</dt>
                <dd><?= !empty($father->fatherSuffix) ? $father->fatherSuffix : 'Not provided'; ?></dd>

                <dt>Date of Birth</dt>
                <dd><?= !empty($father->fatherDateBirth) ? $father->fatherDateBirth : 'Not provided'; ?></dd>

                <dt>Birth Place</dt>
                <dd><?= !empty($father->fatherBirthPlace) ? $father->fatherBirthPlace : 'Not provided'; ?></dd>

                <dt>Certifier Name</dt>
                <dd><?= !empty($father->fatherCertifierName) ? $father->fatherCertifierName : 'Not provided'; ?></dd>



                <dt>Date certified</dt>
                <dd><?= !empty($father->fatherDateCertified) ? $father->fatherDateCertified : 'Not provided'; ?></dd>

                <dt>Date register</dt>
                <dd><?= !empty($father->fatherDateRegister) ? $father->fatherDateRegister : 'Not provided'; ?></dd>

                <dt>Telephone</dt>
                <dd><?= !empty($father->phoneNo) ? $father->phoneNo : 'Not provided'; ?></dd>

                <dt>Referring Hospital</dt>
                <dd><?= !empty($father->referringHospital) ? $father->referringHospital : 'Not provided'; ?></dd>



                <?php
                $titles = null;
                $total_titles = 0;

                if (array_key_exists('titles', $_POST)){
                    $titles = $_POST['titles'];
                    $total_titles = count($titles);
                }


                if (is_null($titles)){
                    echo "<dt>Titles</dt>";
                    echo "you do not chosen any title";
                    return;
                }
                if ($total_titles == 1){
                    echo "<dt>Titles</dt>";
                    echo "your title is ".$titles[0].'.';
                    return;
                }
                if ($total_titles > 1){
                    echo "<dt>Titles</dt>";
                    echo "your titles are ".implode(", ",$titles).'.';
                }
                ?>

            </dl>
       </div>





    </div>


</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>