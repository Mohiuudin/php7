1.variable-handling-functions
a.empty
b.is_array 
c.is_null
d.isset
e.print_r
f.serialize 
g.var_dump
h.array_key_exists
i.unset 
j.gettype
k.is_int

2.string-functions:
a.echo
b.explode 
c.implode 
d.strlen
e.strpos
f.strtolower 
g.strtoupper
h.substr
i.md5
j.nl2br
k.str_repeat
l.str_replace
m.trim
n.ucfirst


https://www.php.net/manual/en/function.empty.php