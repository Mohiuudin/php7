<?php

$str = 'one|two|three|four';

echo "<br>";
//positive limit

print_r(explode('|', $str, 2));
echo "<br>";

//negative limit(Since PHP 5.1)

print_r(explode('|', $str, -1));

?>

Array ( [0] => one [1] => two|three|four )
Array ( [0] => one [1] => two [2] => three ) 
