<?php

$expected_array_got_string = 'somestring';


var_dump(isset($expected_array_got_string['some_key']));

echo "<br>";

var_dump(isset($expected_array_got_string[0]));
echo "<br>";

var_dump(isset($expected_array_got_string['0']));
echo "<br>";

var_dump(isset($expected_array_got_string[0.5]));
echo "<br>";

var_dump(isset($expected_array_got_string['0.5']));    
echo "<br>";

var_dump(isset($expected_array_got_string['0 Mostel']));

?>