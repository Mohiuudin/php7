<?php

$var = '';

//This will evaluate to TRUE so the text will be printed.

if(isset($var)){
    echo "This var is set so I will print";
}


//In the next examples we'll use var_dumo to output

//the return value if isset().

$a = "test";
$b = "anothertest";

echo "<br>";


var_dump(isset($a));  //true
var_dump(isset($a , $b)); // true

echo "<br>";


unset($a);

var_dump(isset($a));  //false
var_dump(isset($a , $b)); // false


echo "<br>";


$foo = null;
var_dump(isset($foo));   //false

?>