<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>Email Subscription</title>

<body>

<form action="emailSubscription.php" method="post">
    <div>
        <label for="email">Email</label><br>
        <input
            id="email"
            type="email" 
            name="email" 
            placeholder="Enter Your Email Address"
        >
    </div>
    <div> <button type="submit">Submit</button>

</form>
</body>
</html>
