<?php
$numericNumber = null;
$lowerCase = null;
$upperCase = null;
$passwordLength = null;
$specialChar = null;

if(strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
    if(array_key_exists('password',$_POST)){
        $password = $_POST['password'];
        $passwordArray = str_split($password);

        foreach($passwordArray as $specificLetter){
            if(empty($specificLetter)){
                echo "<br>";
            }
            if(ctype_space($specificLetter)){
                $whiteSpace = true;
            }
            if(is_numeric($specificLetter)){
                $numericNumber = true;
            }
            if(ctype_lower($specificLetter)){
                $lowerCase = true;
            }
            if(ctype_upper($specificLetter)){
                $upperCase = true;
            }
            if(ctype_punct($specificLetter)){
                $specialChar = true;
            }
        }
    }

    function displayError($errorMassage){
        echo "$errorMassage";
        echo "<br>";

    }
    if($numericNumber == false){
        displayError("Password Must Contain At Least One Number.");

    }

    if($lowerCase == false){
        displayError("Must Be Lower Case");
    }
    if($upperCase == false){
        displayError("At Least One Upper Case");
    }
    if(strlen($password) < 7 ){
        displayError("Password Must Contain At Least 8 Characters.");
    }
    if($specialChar == false){
        displayError("Must Be Special Character Like As @&!#");
    }

    if( $numericNumber && $lowerCase && $upperCase && strlen($password) >= 8 && $specialChar ){
        echo "Successfully Submit Your Data";
        echo "<br>";
        echo "Your Password Is : $password";
    }
}
?>