<?php
/**
 * Created by PhpStorm.
 * User: Mr. SHAKIB
 * Date: 04/23/2020
 * Time: 7:20 PM
 */

namespace App;


class Multiply
{
    public $num1 = null;
    public $num2 = null;


    public function __construct($numbers){
        $this->num1 = $numbers['num1'];
        $this->num2 = $numbers['num2'];
    }


    public function multiply(){
        return $this->num1 + $this->num2;
    }
}