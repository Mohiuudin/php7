<?php


namespace App;


class Subtract
{
    public $num1 = null;
    public $num2 = null;


    public function __construct($numbers){
        $this->num1 = $numbers['num1'];
        $this->num2 = $numbers['num2'];
    }


    public function subtract(){
        return $this->num1 - $this->num2;
    }
}