
<?php

include_once('../vendor/autoload.php');

use App\Child;
use App\Mother;
use App\Father;

$child = new Child($_POST);


echo "<h3>Child details</h3>";
echo "Child First name is ".$child->childFirstName;
echo "<br>";
echo "Child Middle name is ".$child->childMiddleName;
echo "<br>";
echo "Child Last name is ".$child->childLastName;
echo "<br>";
echo "Child Suffix name is ".$child->childSuffix;
echo "<br>";
echo "Child Birth Time is ".$child->childTimeBirth;
echo "<br>";

$genders = [];

    if (strtoupper(array_key_exists('genders', $_POST))) {
        $genders = $_POST['genders'];
    }
    if (count($genders) == 0) {
        echo "Choose the child gender";
    }

    foreach ($genders as $gender):
        echo "Child gender is " . $gender;
    endforeach;

echo "<br>";
echo "Child Date of Birth is ".$child->dateOfBirth;
echo "<br>";
echo "Child Facility Name is ".$child->childFacilityName;
echo "<br>";
echo "Child City Town is ".$child->childCityTown;
echo "<br>";
echo "Child Country Birth Town is ".$child->childCountryBirth;


echo "<hr>";

$mother = new Mother($_POST);

echo "<h3>Mother details</h3>";

echo "Mother First name is ".$mother->motherFirstName;
echo "<br>";
echo "Mother Middle name is ".$mother->motherMiddleName;
echo "<br>";
echo "Mother Last name is ".$mother->motherLastName;
echo "<br>";
echo "Mother Suffix name is ".$mother->motherSuffix;
echo "<br>";
echo "Mother Birth Time is ".$mother->motherTimeBirth;
echo "<br>";
echo "Mother Facility is ".$mother->motherFacilityName;
echo "<br>";
echo "Mother City Town is ".$mother->motherCityTown;
echo "<br>";
echo "Mother County Of Birth id ".$mother->motherCountryBirth;
echo "<br>";
echo "Mother Zip Code is ".$mother->motherZipCode;
echo "<br>";

$motherCityLimits = [];

if (strtoupper(array_key_exists('motherCityLimits', $_POST))) {
    $motherCityLimits = $_POST['motherCityLimits'];
}
if (count($motherCityLimits) == 0) {
    echo "Choose the child motherCityLimits";
}

foreach ($motherCityLimits as $motherCityLimit):
    echo "Mother City Limit is " . $motherCityLimit;
endforeach;

echo "<hr>";



$father = new Father($_POST);

echo "<h3>Father details</h3>";

echo "Father First Name is ".$father->fatherFirstName;
echo "<br>";
echo "Father Middle Name is ".$father->fatherMiddleName;
echo "<br>";
echo "Father Last Name id ".$father->fatherLastName;
echo "<br>";
echo "Father Suffix Name is ".$father->fatherSuffix;
echo "<br>";
echo "Father Certifier's Name is ".$father->fatherCertifierName;
echo "<br>";

$titles = null;
$total_titles = null;
if (array_key_exists('titles', $_POST)){
    $titles = $_POST['titles'];
    $total_titles = count($titles);

    if (is_null($titles)){
        echo "Your do not have chosen any Data";
        return;
    }

    if ($total_titles == 1){
        echo "Your chosen Title is " .$titles[0]. ".";
    }
    if ($total_titles > 1){
        echo "Your chosen Title are " .implode(" , " , $titles). " .";
    }

}

?>



