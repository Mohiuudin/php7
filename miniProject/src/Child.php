<?php


namespace App;

class Child
{

    public $childFirstName = null;
    public $childMiddleName = null;
    public $childLastName = null;
    public $childSuffix = null;
    public $childTimeBirth = null;
    public $genders = null;
    public $dateOfBirth = null;
    public $childFacilityName = null;
    public $childCityTown = null;
    public $childCountryBirth = null;

    public function __construct($childInformation)
    {
        $this->childFirstName = $childInformation['childFirstName'];
        $this->childMiddleName = $childInformation['childMiddleName'];
        $this->childLastName = $childInformation['childLastName'];
        $this->childSuffix = $childInformation['childSuffix'];
        $this->childTimeBirth = $childInformation['childTimeBirth'];
        $this->genders = $childInformation['genders'];
        $this->dateOfBirth = $childInformation['dateOfBirth'];
        $this->childFacilityName = $childInformation['childFacilityName'];
        $this->childCityTown = $childInformation['childCityTown'];
        $this->childCountryBirth = $childInformation['childCountryBirth'];
    }

}

