<?php


namespace App;



class Father
{

    public $fatherFirstName = null;
    public $fatherMiddleName = null;
    public $fatherLastName = null;
    public $fatherSuffix = null;
    public $fatherCertifierName = null;
    public $titles = null;



    public function __construct($fatherInformation)
    {
        $this->fatherFirstName = $fatherInformation['fatherFirstName'];
        $this->fatherMiddleName = $fatherInformation['fatherMiddleName'];
        $this->fatherLastName = $fatherInformation['fatherLastName'];
        $this->fatherSuffix = $fatherInformation['fatherSuffix'];
        $this->fatherCertifierName = $fatherInformation['fatherCertifierName'];
        $this->titles = $fatherInformation['titles'];

    }
}



