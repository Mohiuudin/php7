<?php

namespace App;

class Mother
{

    public $motherFirstName = null;
    public $motherMiddleName = null;
    public $motherLastName = null;
    public $motherSuffix = null;
    public $motherTimeBirth = null;
    public $motherFacilityName = null;
    public $motherCityTown = null;
    public $motherCountryBirth = null;
    public $motherZipCode = null;
    public $motherCityLimits = null;

    public function __construct($motherInformation)
    {
        $this->motherFirstName = $motherInformation['motherFirstName'];
        $this->motherMiddleName = $motherInformation['motherMiddleName'];
        $this->motherLastName = $motherInformation['motherLastName'];
        $this->motherSuffix = $motherInformation['motherSuffix'];
        $this->motherTimeBirth = $motherInformation['motherTimeBirth'];
        $this->motherFacilityName = $motherInformation['motherFacilityName'];
        $this->motherCityTown = $motherInformation['motherCityTown'];
        $this->motherCountryBirth = $motherInformation['motherCountryBirth'];
        $this->motherZipCode = $motherInformation['motherZipCode'];
        $this->motherCityLimits = $motherInformation['motherCityLimits'];
    }
    

}