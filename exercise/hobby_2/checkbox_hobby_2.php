<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>checkbox</title>

<body>
<section>
    <h1>Collect User's Hobby</h1>
    <form action="checkbox_processor_2.php" method="post">
        <fieldset>
            <legend><strong>Please Choose Your Hobbies</strong></legend>




            <input type="checkbox" id="reading_novels" name="hobbies[]" value="Reading Novels">
            <label for="reading_novels">Reading Novels</label>


            <input type="checkbox" id="writing" name="hobbies[]" value="Writing">
            <label for="writing">Writing</label>

            <input type="checkbox" id="watching_movies" name="hobbies[]" value="Watching Movies" >
            <label for="watching_movies">Watching Movies</label>

            <input type="checkbox" id="swimming" name="hobbies[]" value="Swimming">
            <label for="swimming">Swimming</label>


            <br>
            <br>
            <div>
                <button type="submit">Submit</button>
            </div>


        </fieldset>
    </form>
</section>
</body>
</html>