<?php

include_once ('vendor/autoload.php');


use App\Persons\Child;
use App\Persons\Mother;
use App\Persons\Father;

use App\Utility\Sanitizer;
use App\Utility\Validator;
use App\Utility\Debugger;
use App\Utility\Utility;

//$sanitizer = new Sanitizer();
//$validator = new Validator();
//$debugger = new Debugger();
//$utility = new Utility();



if (!Utility::isPosted()){
header("location:form.php");

}
    $sanitizedData = Sanitizer::sanitize($_POST);
    $validatedData = Validator::validate($sanitizedData);

if ($validatedData['validation_success']){

        $child = new Child($validatedData['data']);
        $mother = new Mother($validatedData);
        $father = new Father($validatedData);
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,
initial-scale=1, shrink-to-fit=no">

<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">-->

    <title>Document</title>
</head>

<body>
<?php
if(!$validatedData['validation_success']) {

    foreach ($validatedData['data'] as $error){
        echo $error.'<br>';
    }
}
?>

<div class="container">

        <div class="row">
            <div class="col-md-6">
                <h2>Child details</h2>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="childFirstName">First name</label>
                        <p><strong><?= !empty($child->childFirstName) ? $child->childFirstName : 'Not Provided';?></strong></p>

                    </div>
                    <div class="form-group col-md-6">
                        <label for="childMiddleName">Middle name</label>
                        <p><strong><?= !empty($child->childMiddleName) ? $child->childMiddleName : 'Not Provided';?></strong></p>
                    </div>
                </div>
                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="childLastName">Last name</label>
                                        <p><strong><?= !empty($child->childLastName) ? $child->childLastName : 'Not Provided';?></strong></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="childSuffix">Suffix</label>
                                        <p><strong><?= !empty($child->childSuffix) ? $child->childSuffix : 'Not Provided';?></strong></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="childTimeBirth">Time of birth(24hour)</label>
                                    <p><strong><?= !empty($child->childTimeBirth) ? $child->childTimeBirth : 'Not Provided';?></strong></p>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="gender">Sex</label>
                                        <p><?= !empty($child->genders) ? $child->genders : 'Not Provided';?></p
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="dateOfBirth">Date of Birth</label>
                                        <p><strong><?= !empty($child->dateOfBirth) ? $child->dateOfBirth : 'Not Provided';?></strong></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="childFacilityName">Facility name</label>
                                    <p><strong><?= !empty($child->childFacilityName) ? $child->childFacilityName : 'Not Provided';?></strong></p>
                                </div>
                                <div class="form-group">
                                    <label for="childCityTown">City town</label>
                                    <p><strong><?= !empty($child->childCityTown) ? $child->childCityTown : 'Not Provided';?></strong></p>
                                </div>
                                <div class="form-group">
                                    <label for="childCountryBirth">Country of birth</label>
                                    <p><strong><?= !empty($child->childCountryBirth) ? $child->childCountryBirth : 'Not Provided';?></strong></p>
                                </div>
            </div>

            <hr>

            <div class="col-md-6">
                <h2>Mother details</h2>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="motherFirstName">First name</label>
                        <p><strong><?= !empty($mother->motherFirstName) ? $mother->motherFirstName : 'Not Provided';?></strong></p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="motherMiddleName">Middle name</label>
                        <p><strong><?= !empty($mother->motherMiddleName) ? $mother->motherMiddleName : 'Not Provided';?></strong></p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="motherLastName">Last name</label>
                        <p><strong><?= !empty($mother->motherLastName) ? $mother->motherLastName : 'Not Provided';?></strong></p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="motherSuffix">Suffix</label>
                        <p><strong><?= !empty($mother->motherSuffix) ? $mother->motherSuffix : 'Not Provided';?></strong></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="motherTimeBirth">Time of birth(24hour)</label>
                    <p><strong><?= !empty($mother->motherTimeBirth) ? $mother->motherTimeBirth : 'Not Provided';?></strong></p>
                </div>

                <div class="form-group">
                    <label for="motherFacilityName">Facility name</label>
                    <p><strong><?= !empty($mother->motherFacilityName) ? $mother->motherFacilityName : 'Not Provided';?></strong></p>
                </div>
                <div class="form-group">
                    <label for="motherCityTown">City town</label>
                    <p><strong><?= !empty($mother->motherCityTown) ? $mother->motherCityTown : 'Not Provided';?></strong></p>
                </div>
                <div class="form-group">
                    <label for="motherCountryBirth">Country of birth</label>
                    <p><strong><?= !empty($mother->motherCountryBirth) ? $mother->motherCountryBirth : 'Not Provided';?></strong></p>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="motherZipCode">Zip Code</label>
                        <p><strong><?= !empty($mother->motherZipCode) ? $mother->motherZipCode : 'Not Provided';?></strong></p>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="form-check">
                            <p>Inside city limits</p>
                            <p><strong><?= !empty($mother->motherCityLimits) ? $mother->motherCityLimits : 'Not Provided';?></strong></p>
                            <label class="form-check-label"
                                   for="exampleRadios1">
                                Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <p><strong><?= !empty($mother->motherCityLimits) ? $mother->motherCityLimits : 'Not Provided';?></strong></p>
                            <label class="form-check-label"
                                   for="exampleRadios2">
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
         
        </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h2>Father details</h2>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="fatherFirstName"></label>
                    <p><strong><?= !empty($father->fatherFirstName) ? $father->fatherFirstName : 'Not Provided';?></strong></p>
                </div>
                <div class="form-group col-md-6">
                    <label for="fatherMiddleName">Middle name</label>
                    <p><strong><?= !empty($father->fatherMiddleName) ? $father->fatherMiddleName : 'Not Provided';?></strong></p>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="fatherLastName">Last name</label>
                    <p><strong><?= !empty($father->fatherLastName) ? $father->fatherLastName : 'Not Provided';?></strong></p>
                </div>
                <div class="form-group col-md-6">
                    <label for="fatherSuffix">Suffix</label>
                    <p><strong><?= !empty($father->fatherSuffix) ? $father->fatherSuffix : 'Not Provided';?></strong></p>
                </div>
            </div>
            <div class="form-group">
                <label for="fatherCertifierName">Father
                    certifier's name</label>
                <p><strong><?= !empty($father->fatherCertifierName) ? $father->fatherCertifierName : 'Not Provided';?></strong></p>

            </div>

            <div class="form-group">
                <p>title</p>
                <input type="checkbox" id="fatherTitleMd"
                       name="titles[]" value="md">
                <label for="fatherTitleMd">MD</label>

                <input type="checkbox" id="fatherTitleDo"
                       name="titles[]" value="do">
                <label for="fatherTitleDo">DO</label>

                <input type="checkbox"
                       id="fatherTitleDoHospitalAdmin" name="titles[]" value="hospital
admin">
                <label for="fatherTitleDoHospitalAdmin">hospital
                    admin</label>
                <input type="checkbox" id="fatherTitleCnm"
                       name="titles[]" value="cnm/cm">
                <label for="fatherTitleCnm">CNM/CM</label>
                <input type="checkbox" id="fatherTitleOthers"
                       name="titles[]" value="others">
                <label for="fatherTitleOthers">Others</label>




            </div>
        </div>
    </div>


</div>



</body>

</html>


