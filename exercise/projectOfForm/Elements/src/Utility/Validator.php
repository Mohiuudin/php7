<?php

namespace App\Utility;

class Validator{

    
    
    public static function validate($nonValidatedData){

    $errors = [];
    $validatedDate = null;;

        if ($nonValidatedData['childFirstName'] == ''){
            $errors[] = "Your Child first Name is not found. Please Provide First Name";
        }
        
        if ($nonValidatedData['childLastName'] == ''){
            $errors[] = "Your Child Last Name is not found. Please Provide Last Name";
        }

       if (count($errors) > 0){
           return ['validation_success' => false, 'data' => $errors];
       }
        $validatedDate =  $nonValidatedData;
        return ['validation_success' => true, 'data' => $validatedDate];
    }
}