<?php

namespace App;


//$result = "";

class Calculator
{
    var $a;
    var $b;

    function checkOperation($operator)
    {
        switch($operator)
        {
            case '+':
                return $this->a + $this->b;
                break;

            case '-':
                return $this->a - $this->b;
                break;

            case '*':
                return $this->a * $this->b;
                break;

            case '/':
                return $this->a / $this->b;
                break;

            default:
                return "Sorry No command found";
        }
    }
    function getResult($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        return $this->checkOperation($c);
    }
}
