<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>YesNoMaybe</title>

<body>
<section>
<form action="processor.php" method="post">
    <fieldset>
        <legend>Are You Going</legend>
        <div>
            <label for="are_you_going_yes">Yes</label>
            <input type="radio" id="are_you_going_yes" name="are_you_going" value="yes" checked="checked">

            <label for="are_you_going_no">No</label>
            <input type="radio" id="are_you_going_no" name="are_you_going" value="no" checked="checked">

            <label for="are_you_going_maybe">May Be</label>
            <input type="radio" id="are_you_going_maybe" name="are_you_going" value="may be" checked="checked">

        </div>
        <br>
        <div> <button type="submit">Submit</button>

    </fieldset>
</form>
</section>
</body>
</html>