<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>radioPreferred</title>

<body>

<form action="processor.php" method="post">
    <fieldset>
        <legend>What is your preferred contact method</legend>
    <div>
        <label for="email">Email</label>
        <input type="radio" id="email" name="contact" value="email">

        <label for="phone">Phone</label>
        <input type="radio" id="phone" name="contact" value="phone">

        <label for="mail">Mail</label>
        <input type="radio" id="mail" name="contact" value="mail">

    </div>
        <br>
        <div> <button type="submit">Submit</button>

    </fieldset>
</form>
</body>
</html>

