<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>Toggle</title>

<body>
<section>
    <h1>Toggle Yes/No</h1>
    <form action="processor.php" method="post">
        <fieldset>
            <legend>Toggle -Yes/No</legend>

            <div>Are you agreed to the terms and condition?</div>


                <input type="radio" id="toggle_yes" name="toggle" value="1"">
            <label for="toggle_yes">Yes</label>


            <input type="radio" id="toggle_no" name="toggle" value="0"">
            <label for="toggle_no">No</label>

            
            <br>
            <br>
            <div>
                <button type="submit">Submit</button>
            </div>
           

        </fieldset>
    </form>
</section>
</body>
</html>